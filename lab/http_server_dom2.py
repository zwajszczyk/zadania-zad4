# -*- encoding: utf-8 -*-
__author__ = 'Zbyszek'

import socket
import email.utils
import os
import logging
import time
import errno
from daemon import runner


def http_serve(server_socket):
    base_path = '/home/p18/web' #sciezka domyslnego katalogu, wzgledem ktorego wyznaczamy adresy
    while True:
        # Czekanie na polaczenie
        connection, client_address = server_socket.accept()
        try:
            # Odebranie zadania
            try:
                request = connection.recv(1024)
            except:
                logger.error('nie udal sie request')
            if request:
                requesttable = request.split("\r\n") #dzielimy żądanie
                first_line_req = requesttable[0].split(" ") # i pierwszą linię żądania
                #warunek sprawdzający poprawność żądania HTTP
                if len(first_line_req)==3 and (str(first_line_req[0])=="GET") and (str(first_line_req[2][:4])=="HTTP"):
                    uri = first_line_req[1]
                    #logger.info(request) - tymczasowo
                    if os.path.exists(base_path+uri): #sprawdzamy czy istnieje zasób do któego chcemy się odwołać
                        header_status = "HTTP/1.1 200 OK\r\n" #zasób istnieje, zwracamy sukces
                        if uri[-5:]=='.html' or uri[-4:]=='.htm': #dla dokumentu html
                            html = open(base_path+uri).read() #dane
                            header_contenttype="Content-Type: text/html; charset=UTF-8\r\n" #MIME Type danych - odpowiedni header
                            header_length="Content-Length: "+ str(len(html))+"\r\n" # długość danych
                        elif uri[-4:]=='.png': # dla obrazu .png
                            html = file(base_path+uri, 'rb').read() # czytamy binarnie
                            header_contenttype="Content-Type: image/png\r\n"
                            header_length="Content-Length: "+ str(len(html))+"\r\n"
                        elif uri[-4:]=='.txt': # dla plku tekstowego
                            html = open(base_path+uri).read()
                            header_contenttype="Content-Type: text/plain; charset=UTF-8\r\n"
                            header_length="Content-Length: "+ str(len(html))+"\r\n"
                        elif uri[-4:]=='.jpg' or uri[-5:]=='.jpeg': # dla obrazu jpg
                            html = file(base_path+uri, 'rb').read()
                            header_contenttype="Content-Type: image/jpeg\r\n"
                            header_length="Content-Length: "+ str(len(html))+"\r\n"
                        elif os.path.isdir(base_path+uri): # dla folderu
                            html='<html><head></head><body><ul>' # zawartośc zostanie wylistowana
                            dir_list = os.listdir(base_path+uri) # zawartosc folderu
                            #print dir_list
                            for filename in dir_list:
                                if os.path.isdir(base_path+uri+filename): #foldery wypisujemy z / na końcu
                                    html+='<li><a href="'+uri+filename+'/">'+filename+'/</a></li>'
                                else:
                                    html+='<li><a href="'+uri+filename+'">'+filename+'</a></li>'
                            html+='</ul></body></html>'
                            header_contenttype="Content-Type: text/html; charset=UTF-8\r\n"
                            header_length="Content-Length: "+ str(len(html))+"\r\n"
                        else: # dla nieobsługiwanego typu danych - traktuje jako plik który mozna ściągnąć
                            html = file(base_path+uri, 'rb').read()
                            header_contenttype="Content-Type: application/octet-stream\r\n"
                            header_length="Content-Length: "+ str(len(html))+"\r\n"
                    else: # dla nieistniejacego zasobu
                        logger.warning("HTTP/1.1 404 Not Found\r\n")
                        header_status = "HTTP/1.1 404 Not Found\r\n"
                        html = '<html><head></head><body><h4>404 Not Found</h4><p>Nie ma takiego numeru. Zasobu też.</p></body></html>'
                        header_contenttype="Content-Type: text/html; charset=UTF-8\r\n"
                        header_length="Content-Length: "+ str(len(html))+"\r\n"
                else: # dla błędnego requestu
                    logger.warning('HTTP/1.1 400 Bad Request\r\n')
                    header_status = "HTTP/1.1 400 Bad Request\r\n"
                    html = "Uzytkowniku, jestes bardzo zlym czlowiekiem. To jest serwer HTTP i przyjmuje zadania HTTP. Prosimy do okienka nr 4b."
                    header_contenttype="Content-Type: text/html; charset=UTF-8\r\n"
                header_date ="GMT-Date: "+email.utils.formatdate(usegmt=True)+"\r\n" # data w odpowiednim formacie
                # wysyłamy nagłówki + dane
                try:
                    connection.sendall(header_status+header_contenttype+header_date+header_length+"\r\n"+html)
                    logger.info(header_status+header_contenttype+header_date+header_length+"\r\n")
                except socket.error: #zabezpieczenie przed Broken Pipe
                    logger.critical('Broken pipe')
                    continue
		except:
			logger.error('cos sie psuje')
        finally:
            # Zamkniecie polaczenia
			logger.info('zamykamy')
			connection.close()


class App:
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path = '/home/p18/demonp18_new.pid'
        self.pidfile_timeout = 5

    # Główny kod aplikacji
    def run(self):
        # Tworzenie gniazda TCP/IP
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Ustawienie ponownego uzycia tego samego gniazda
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # Powiązanie gniazda z adresem
        #server_address = ('localhost', 80)  # wersja działająca na localhoście
        server_address = ('194.29.175.240', 31018) # wersja mogaca działać (i działająca) na serwerze
        server_socket.bind(server_address)

        # Nasłuchiwanie przychodzących połączeń
        server_socket.listen(1)

        try:
            http_serve(server_socket)
        except Exception, exc:
            logger.error(exc.message);
        finally:
            server_socket.close()
        logger.warning('zamkniecie bez wyjatku')

# Ustawienie loggera
logger = logging.getLogger("DemonLog")
logger.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("demon.log")
handler.setFormatter(formatter)
logger.addHandler(handler)

# Stworzenie i uruchomienie aplikacji jako demona
app = App()
try:
    daemon_runner = runner.DaemonRunner(app)
except Exception, exc:
    logger.error(exc.message);
# Zapobiegnięcie zamknięciu pliku logów podczas demonizacji
daemon_runner.daemon_context.files_preserve = [handler.stream]
try:
    daemon_runner.do_action()
except Exception, exc:
    logger.error(exc.message);






