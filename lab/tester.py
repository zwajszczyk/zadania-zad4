# -*- encoding: utf-8 -*-
__author__ = 'Zbyszek'

from funkload.FunkLoadTestCase import FunkLoadTestCase
import os
import socket
import unittest

#SERVER_HOST = os.environ.get('KATECHIZM_SERVER', 'localhost')


class TestSerweraHTTP(FunkLoadTestCase):

    base_path='127.0.0.1'
    base_port = 80
    qa = (
        ('GET / HTTP/1.1\r\n', '200'),
        ('GET /web_page.html HTTP/1.1\r\n', '200'),
        ('GET /lokomotywa.txt HTTP/1.1\r\n', '200'),
        ('GET /images/gnu_meditate_levitate.png HTTP/1.1\r\n', '200'),
        ('GET /dupa.html HTTP/1.1\r\n', '404'),
        ('hej hej bumtarara\r\n', '400')
    )

    def test_dialog(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.base_path, self.base_port))
        for i in (1,len(self.qa)):
            question, answer = self.qa[i]
            sock.sendall(question)
            reply = self.recv_until(sock, ['\r\n'])

            self.assertEqual(reply, answer)
        sock.close()

    def ends_with(self, message, suffixes):
        for suffix in suffixes:
            if message.endswith(suffix):
                return True
        return False

    def recv_until(self, sock, suffixes):
        message = ''
        while not self.ends_with(message, suffixes):
            data = sock.recv(4096)
            if not data:
                raise EOFError('Gniazdo zamknięte przed otrzymaniem jednego z %r możliwych zakończeń' % ','.join(suffixes))
            message += data
        return message



if __name__ == '__main__':
    unittest.main()